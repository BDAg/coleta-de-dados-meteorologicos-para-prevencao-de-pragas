<h3>Sobre:</h3><br>
<center>O Brasil possui diversas estações meteorológicas distribuídas pelo país  e essas estações pertencem ao instituto nacional de meteorologia(INMET) que fornecem dados em tempo real dos sensores instalados nesses equipamentos. O crescimento das informações meteorológicas auxiliam diretamente o campo para aplicação de defensivos agrícolas. 
</center>

<h3>Projeto</h3>
Abaixo, você encontrará todos os arquivos necessários para o desenvolvimento do projeto.<br/>
Lista de Funcionalidades: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Lista-de-funcionalidades)<br> 
Project Charter: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Project-Charter)<br/>
Cronograma: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Cronograma)<br/>
Avaliação: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Matriz-de-habilidades)<br/>
Mapa do Conhecimento: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Mapa-do-Conhecimento)

<h3>Relatório final</h3>
Relatório do projeto: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Relat%C3%B3rio)


<h3>Currículo</h3>
David Garajan Rohwedder: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/David-Garajan-Rohwedder)<br>
Eduardo Vinicius Batista: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Eduardo-Vinicius-Batista)<br>
João Vitor Pereira Cavalcante: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Jo%C3%A3o-Vitor-Pereira-Cavalcante)<br>
José Gabriel Madureira Adami: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Jos%C3%A9-Gabriel-Madureira-Adami)<br>
Ulisses Stefano Rodrigues Ramos: [Link](https://gitlab.com/BDAg/coleta-de-dados-meteorologicos-para-prevencao-de-pragas/wikis/Ulisses-Stefano-Rodrigues-Ramos)<br>